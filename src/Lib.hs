{-# LANGUAGE
  DeriveGeneric
, DeriveAnyClass
, OverloadedStrings
#-}

module Lib where

import GHC.Generics
import           Data.Aeson
import qualified Data.ByteString.Lazy as BL
import           Text.Regex.TDFA
import System.Directory
import System.Process
import System.Environment
import qualified Data.Text as T
import qualified Data.List as L


data Engines = Engines
  { engines :: Engine
  }
  deriving (Generic, Show, FromJSON, ToJSON)

data Engine = Engine
  { node :: String
  , npm :: Maybe String
  }
  deriving (Generic, Show, FromJSON, ToJSON)

shellType :: IO String
shellType = do
  shellPath <- getEnv "SHELL"
  let shell = L.last . T.splitOn (T.pack "/") $ (T.pack shellPath)
  return $ T.unpack shell

packageJson :: String
packageJson = "package.json"

parseEngines :: IO Engines
parseEngines = do
  fileExist <- doesFileExist packageJson
  if fileExist then do
    file <- BL.readFile packageJson
    let eitherEngines = eitherDecode file :: Either String Engines
    decodedEngines <- case eitherEngines of
      Left err -> error err
      Right x -> return x
    return decodedEngines
  else
    return $ Engines (Engine "" (Just ""))

getNodeVersion :: IO Engines -> IO String
getNodeVersion engines = do
  Engines (Engine node _) <- engines
  let version = node =~ ("([0-9]|(\\.))+" :: String)
  return version

createNvmrcFile :: IO ()
createNvmrcFile = do
  homeFolder <- getHomeDirectory
  filePath <- getCurrentDirectory
  nodeVersion <- getNodeVersion parseEngines
  createFile filePath nodeVersion
  where
    createFile filePath nodeVersion = do
      hasNvmrc <- doesFileExist $ filePath <> "/.nvmrc"
      if hasNvmrc then
        return ()
      else
        writeFile (filePath <> "/.nvmrc") nodeVersion

createShellScript :: IO ()
createShellScript = do
  homeDir <- getHomeDirectory
  hasNvmScriptInHome <- doesFileExist $ homeDir <> "/.nvm/nvm.sh"
  hasNvmScriptInUsr <- doesFileExist "/usr/local/opt/nvm/nvm.sh"
  shell <- shellType
  hasBashrc <- doesFileExist $ homeDir <> "/.bashrc"
  hasZshrc <- doesFileExist $ homeDir <> "/.zshrc"

  -- TODO: check if script already exist in the file. Plus this bunch of if/else looks ugly.

  if hasBashrc && hasZshrc then do
    appendFile (homeDir <> "/.bashrc") (generateScript hasNvmScriptInHome hasNvmScriptInUsr homeDir)
    appendFile (homeDir <> "/.zshrc") (generateScript hasNvmScriptInHome hasNvmScriptInUsr homeDir)
    feedbackMessage shell homeDir
  else if hasBashrc then do
    appendFile (homeDir <> "/.bashrc") (generateScript hasNvmScriptInHome hasNvmScriptInUsr homeDir)
    feedbackMessage shell homeDir
  else if hasZshrc then do
    appendFile (homeDir <> "/.zshrc") (generateScript hasNvmScriptInHome hasNvmScriptInUsr homeDir)
    feedbackMessage shell homeDir
  else
    putStrLn $
      ".bashrc / .zshrc not found. Make sure you have "
      <> "either of those files in your home directory."
  where
    generateScript hasNvmScriptInHome hasNvmScriptInUsr homeDir =
      if hasNvmScriptInUsr then
          "\n[ -s \"/usr/local/opt/nvm/nvm.sh\" ] && . \"/usr/local/opt/nvm/nvm.sh\""
          <> "\nfunction cdp() { cd $1 && cdpcli --create-nvmrc ; nvm use }\n"
      else if hasNvmScriptInHome then
          "\n[ -s \"" <> homeDir <> "/.nvm/nvm.sh\" ] && . \"" <> homeDir <> "/.nvm/nvm.sh\""
          <> "\nfunction cdp() { cd $1 && cdpcli --create-nvmrc ; nvm use ; }\n"
        else
          ""
    feedbackMessage shell homeDir =
      case shell of
        "bash" -> putStrLn $ "Please reload your shell with: source " <> homeDir <> "/.bashrc"
        "zsh" -> putStrLn $ "Please reload your shell with: source " <> homeDir <> "/.zshrc"
        _ -> putStrLn "Make sure you SHELL in your environment is either 'bash' or 'zsh'"

install :: IO ()
install = do
  homeDir <- getHomeDirectory
  currentDir <- getCurrentDirectory
  hasLocalDir <- doesDirectoryExist $ homeDir <> "/myLocal"
  hasBinDir <- doesDirectoryExist $ homeDir <> "/myLocal/bin"
  createShellScript
  createDirectoryRecursively [homeDir, "myLocal", "bin"]
  copyFile (currentDir <> "/cdpcli") (homeDir <> "/myLocal/bin/cdpcli")
  appendFile (homeDir <> "/.bashrc") ("export PATH=$PATH:" <> homeDir <> "/myLocal/bin")
  putStrLn $ "'cdpcli' has been copied to '" <> homeDir <> "/myLocal/bin' folder.\nDo you want to delete this original dowload? (y|n)"
  answer <- getLine
  if answer == "y" || answer == "Y" then do
    removeFile $ currentDir <> "/cdpcli"
    putStrLn "File deleted with success."
  else do
    return ()

createDirectoryRecursively :: [String] -> IO ()
createDirectoryRecursively xs = run xs []
  where
    run [] _ = do
      return ()
    run (x:xs) ys = do
      let path = L.intercalate "/" $ ys <> [x]
      folderExist <- doesDirectoryExist path
      if folderExist then do
        run xs $ ys <> [x]
      else do
        createDirectory path
        run xs $ ys <> [x]
