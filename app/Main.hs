module Main where

import           Lib
import           System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["--install"] -> install

    ["--create-nvmrc"] -> createNvmrcFile

    _ -> putStrLn $
      "Options are:\n"
      <> " cdpcli --install\n"
